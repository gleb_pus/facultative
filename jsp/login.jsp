<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<title>Login JSP</title>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="login.loginButton" var="loginButton" />
<fmt:message bundle="${loc}" key="login.login" var="login" />
<fmt:message bundle="${loc}" key="login.password" var="password" />
<fmt:message bundle="${loc}" key="login.registration" var="registration" />
</head>
	<body bgcolor="#CAFF70">
		<hr>
		<form name="loginForm" method="POST" action="${pageContext.request.contextPath}/Controller">
			<input type="hidden" name="command" value="login" /> ${login}:<br /> 
			<input type="text" name="login" value="" /> <br />${password}:<br />
			<input type="password" name="password" value="" /> <br /> 
			${errorMessage} <br /> 
			<input type="submit" name="login" value="${loginButton}" />
		</form>
		<a href="/facultative/jsp/registration.jsp">${registration}</a>
		<hr>
	</body>
</html>