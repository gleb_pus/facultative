<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="course" scope="request" class="com.epam.entity.Course" />
<html>
<head>
<title>Teacher JSP</title>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/style/userStyle.css" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="teacher.students.message" var="message" />
<fmt:message bundle="${loc}" key="teacher.student.name" var="name" />
<fmt:message bundle="${loc}" key="teacher.student.surname" var="surname" />
<fmt:message bundle="${loc}" key="teacher.student.rate" var="rate" />
<fmt:message bundle="${loc}" key="teacher.button.review" var="reviewStudent" />
<fmt:message bundle="${loc}" key="teacher.button.editreview" var="editreview" />
<fmt:message bundle="${loc}" key="teacher.button.return" var="returnToList" />
<fmt:message bundle="${loc}" key="teacher.review.review" var="review" />
<fmt:message bundle="${loc}" key="empty.students" var="emptyStudents" />
</head>
<body bgcolor="#CAFF70">
	<div class="yellowBlock">
		<h2>${message} "${course.name}"</h2></br>
	</div>
	<div class="greenBlock">
		<c:choose>
			<c:when test="${not empty students}">
				<table class="tableStyle">
					<tr>
						<th>${name}</th>
						<th>${surname}</th>
						<th>${rate}</th>
						<th>${review}</th>
					</tr>	
						<c:forEach var="student" items="${students}">
							<c:set var="rate" value="" />
							<c:set var="hasReview" value="false" />
							<c:set var="courseId" value="${course.id}" />			
								<c:if test="${not empty student.reviews}">
									<c:forEach var="studentReview" items="${student.reviews}">	
										<c:if test="${studentReview.courseId eq courseId}">
											<c:set var="rate" value="${studentReview.rate}" />
											<c:set var="review" value="${studentReview.review}" />
											<c:set var="hasReview" value="true" />
										</c:if>
									</c:forEach>
								</c:if>
								<c:choose>
									<c:when test="${hasReview eq 'true' }">
										<tr class="tr1">
											<td><c:out value="${ student.firstName }" /></td>
											<td><c:out value="${ student.lastName}" /></td>
											<td><c:out value="${rate}" /></td>
											<td><c:out value="${review}" /></td>
											<td class="message">
												<c:url value="/Controller" var="url">
													<c:param name="courseId" value="${courseId}" />
													<c:param name="userId" value="${student.id}" />
													<c:param name="command" value="goToEditReview" />
													<c:param name="rate" value="${rate}" />
													<c:param name="review" value="${review}" />
												</c:url> 
												<a href="${url}">${editreview}</a>
											</td>
										</tr>
									</c:when>			
									<c:when test="${hasReview eq 'false' }">
										<tr>
											<td><c:out value="${ student.firstName }" /></td>
											<td><c:out value="${ student.lastName}" /></td>
											<td>-</td>
											<td>-</td>
											<td class="message">
												<c:url value="/Controller" var="url">
													<c:param name="courseId" value="${courseId}" />
													<c:param name="userId" value="${student.id}" />
													<c:param name="command" value="goToReview" />
												</c:url> 
												<a href="${url}">${reviewStudent}</a></td>
										</tr>
									</c:when>
								</c:choose>
						</c:forEach>
				</table></br>
			</c:when>
			<c:when test="${empty students}">
				<h2>${emptyStudents}</h2>
			</c:when>
		</c:choose>
			<h2><a href="/facultative/jsp/teacher/teacherCourses.jsp">${returnToList}</a></h2>
	</div>
</body>
</html>
