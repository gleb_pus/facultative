<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="/WEB-INF/tld/taglib.tld" prefix="mytag"%>
<jsp:useBean id="user" scope="session" class="com.epam.entity.User" />
<html>
<head>
<title>Teacher JSP</title>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/style/userStyle.css" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="teacher.message1" var="message1" />
<fmt:message bundle="${loc}" key="teacher.message2" var="message2" />
<fmt:message bundle="${loc}" key="main.begin" var="begin" />
<fmt:message bundle="${loc}" key="teacher.empty.courses" var="emptyCourses" />
</head>
<body bgcolor="#CAFF70">
	<div class="yellowBlock">
		<h2>${message1} ${user.firstName} ${user.lastName}!</h2>
		<h3>${message2}</h3></br>		
	</div>
	<div class="greenBlock">
		<c:choose>
			<c:when test="${not empty user.courses}">
				<c:forEach var="course" items="${user.courses}">
					<c:url value="/Controller" var="url">
						<c:param name="courseId" value="${course.id}" />
						<%-- <c:param name="courseName" value="${course.name}" />--%>
						<c:param name="command" value="teacher" />
					</c:url>
					<h2><a href="${url}">${course.name}</a></h2>
					<h5><c:out value="${begin}: ${course.beginCourse}" /></h5>
					<hr>
				</c:forEach>
			</c:when>
			<c:when test="${empty user.courses }">
				<h2>${emptyCourses}</h2>
			</c:when>
		</c:choose>
	</div>
</body>
</html>

