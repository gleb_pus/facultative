<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="course" scope="request" class="com.epam.entity.Course" />
<jsp:useBean id="student" scope="request" class="com.epam.entity.Student" />
<html>
<head>
<title>Teacher JSP</title>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/style/userStyle.css" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="teacher.review.student" var="studentMessage" />
<fmt:message bundle="${loc}" key="teacher.review.rate" var="rate" />
<fmt:message bundle="${loc}" key="teacher.review.review" var="review" />
<fmt:message bundle="${loc}" key="teacher.review.send" var="send" />
<fmt:message bundle="${loc}" key="teacher.review.saveEdit" var="saveEdit" />
<fmt:message bundle="${loc}" key="teacher.button.return" var="returnToListCourse" />
</head>
<body bgcolor="#CAFF70">
	<div class =yellowBlock>
		<h2>${studentMessage} ${student.firstName} ${student.lastName} ${course.id}  ${student.id}</h2>
	</div>
	<div class ="greenBlock">
		<form name="editForm" method="POST" action="${pageContext.request.contextPath}/Controller">
			<input type="hidden" name="command" value="editReview" />
			<input type="hidden" name="courseId" value="${course.id}" />
			<input type="hidden" name="userId" value="${student.id}" />${rate}:<br /> 
			<input type="text" name="rate" value="${param.rate}" /><br /> ${review}:<br /> 
			<textarea name="review" rows="7" cols="30">${param.review}</textarea><br />
			${errorMessage} <br />
			<input type="submit" name="send" value="${saveEdit}" />
		</form>
		<a href="/facultative/jsp/teacher/teacherCourses.jsp">${returnToListCourse}</a>
	</div>
</body>
</html>