<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="mytag" uri="/WEB-INF/tld/taglib.tld" %>
<jsp:useBean id="user" scope="session" class="com.epam.entity.User" />
<jsp:useBean id="course" scope="request" class="com.epam.entity.Course" />
<html>
<head>
<title>Student JSP</title>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/style/userStyle.css">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="main.begin" var="begin" />
<fmt:message bundle="${loc}" key="main.end" var="end" />
<fmt:message bundle="${loc}" key="main.return" var="returnToListCourses" />
<fmt:message bundle="${loc}" key="student.desc.registerButton" var="registerButton" />
<fmt:message bundle="${loc}" key="student.desc.unregButton" var="unregButton" />
</head>
<body bgcolor="#CAFF70">
	<div class="yellowBlock">
		<h2>${course.name}</h2>
	</div></br>	
	<div class="greenBlock">	
		<h3>
			<mytag:dsc description ="${course.description}" /></br>
		</h3>
		<h3>${begin}:${course.beginCourse}|${end}: ${course.endCourse}</h3>
		<c:choose>
			<c:when test="${not empty user.courses }">
				<c:set var="userHasCourse" value="false" />
				<c:forEach var="userCourse" items="${user.courses}">
					<c:if test="${userCourse.id eq course.id }">
						<c:set var="userHasCourse" value="true" />
					</c:if>
				</c:forEach>
				<c:choose>
					<c:when test="${userHasCourse eq 'true' }">
						<c:url value="/Controller" var="url">
							<c:param name="courseId" value="${param.courseId}" />
							<c:param name="command" value="courseUnregister" />
						</c:url>
							<h2><a href="${url}">${unregButton}</a></h2></br>
					</c:when>
					<c:when test="${userHasCourse eq 'false' }">
						<c:url value="/Controller" var="url">
							<c:param name="courseId" value="${param.courseId}" />
							<c:param name="command" value="courseRegister" />
						</c:url>
						<h2><a href="${url}">${registerButton} </a></h2></br>
					</c:when>
				</c:choose>
			</c:when>
			<c:when test="${empty user.courses}">
				<c:url value="/Controller" var="url">
					<c:param name="courseId" value="${param.courseId}" />
					<c:param name="command" value="courseRegister" />
				</c:url>
				<h2><a href="${url}">${registerButton} </a></h2>
			</c:when>
		</c:choose>
		<h3><a href="/facultative/jsp/student/studentCoursesList.jsp">${returnToListCourses}</a></h3>
	</div>
</body>
</html>