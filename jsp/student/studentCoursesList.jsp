<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="user" scope="session" class="com.epam.entity.User" />
<html>
<head>
<title>Student JSP</title>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/style/userStyle.css" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="student.list.message1" var="message1" />
<fmt:message bundle="${loc}" key="student.list.message2" var="message2" />
<fmt:message bundle="${loc}" key="main.begin" var="begin" />
<fmt:message bundle="${loc}" key="student.noCourses" var="noCourses" />
</head>
<body bgcolor="#CAFF70" >
	<div class="yellowBlock">
		<h2>${message1} ${user.firstName} ${user.lastName}!</h2>
		<h3>${message2}</h3>
	</div>
	<div class="greenBlock">
		<c:choose>
			<c:when test="${not empty allCourses}">
				<c:forEach var="allcourse" items="${allCourses}">
					<c:url value="/Controller" var="url">
						<c:param name="courseId" value="${allcourse.id}" />
						<c:param name="command" value="studentCourseDescription" />
					</c:url>
					<h2><a href="${url}">${allcourse.name}</a></h2> 
					<h5><c:out value="${begin}: ${allcourse.beginCourse}" /></h5>
					<hr>
				</c:forEach>
			</c:when>
			<c:when test="${empty allCourses}">
				<h2>${noCourses}</h2>
			</c:when>
		</c:choose>
	</div>
</body>
</html>