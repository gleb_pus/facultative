<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="user" scope="session" class="com.epam.entity.User" />
<jsp:useBean id="course" scope="request" class="com.epam.entity.Course" />
<html>
<head>
<title>Student JSP</title>
<link type="text/css" rel="stylesheet"
	href="${pageContext.request.contextPath}/style/userStyle.css" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="student.reg.message1" var="message1" />
<fmt:message bundle="${loc}" key="main.return" var="returnToListCourses" />
<fmt:message bundle="${loc}" key="student.reg.message2" var="message2" />

</head>
<body bgcolor="#CAFF70">
	<div class="yellowBlock">
		<h2>${message1} "${course.name}"</h2>
	</div>
	<div class="greenBlock">
		</br>
		<form name="loginForm" method="POST" action="Controller">
			<input type="hidden" name="command" value="login" /> 
			<input type="hidden" name="login" value="${user.login}" /> 
			<input type="hidden" name="password" value="${user.password}" /> 
			<input type="submit" name="login" value="${returnToListCourses}" />
		</form>
		<h4>${message2}</h4>
	</div>
</body>
</html>