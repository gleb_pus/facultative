<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="mytag" uri="/WEB-INF/tld/taglib.tld" %>
<jsp:useBean id="user" scope="session" class="com.epam.entity.User" />
<jsp:useBean id="course" scope="request" class="com.epam.entity.Course" />
<html>
<head>
<title>Admin JSP</title>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/style/userStyle.css" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="main.begin" var="begin" />
<fmt:message bundle="${loc}" key="main.end" var="end" />
<fmt:message bundle="${loc}" key="main.return" var="returnToListCourse" />
<fmt:message bundle="${loc}" key="admin.deleteCourse" var="deleteCourse" />
<fmt:message bundle="${loc}" key="admin.editCourse" var="editCourse" />
<fmt:message bundle="${loc}" key="admin.showStudents" var="showStudents" />
</head>
<body bgcolor="#CAFF70">
	<div class="yellowBlock">
		<h2>${course.name}</h2></br>
	</div>
	<div class="greenBlock">
		<h3>
			<mytag:dsc description ="${course.description}" /></br>
		</h3>
		<h3>${begin}: ${course.beginCourse} | ${end}: ${course.endCourse}</h3>
		<c:url value="/Controller" var="url">
			<c:param name="courseId" value="${course.id}" />
			<c:param name="command" value="deleteCourse" />
		</c:url>
		<h2> <a href="${url}">${deleteCourse}</a></h2>
		<c:url value="/Controller" var="url">
			<c:param name="courseId" value="${course.id}" />
			<c:param name="command" value="goToAdminEditCourse" />
		</c:url>	
		<h2><a href="${url}">${editCourse}</a></h2>
		<c:url value="/Controller" var="url">
			<c:param name="courseId" value="${param.courseId}" />
			<c:param name="command" value="showCourseStudents" />
		</c:url>
		<h2><a href="${url}">${showStudents}</a></h2>
		<h2><a href="/facultative/jsp/admin/admin.jsp">${returnToListCourse}</a></h2>
	</div>
</body>
</html>