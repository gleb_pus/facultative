<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="user" scope="session" class="com.epam.entity.User" />
<jsp:useBean id="course" scope="request" class="com.epam.entity.Course" />
<html>
<head>
<title>Admin JSP</title>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/style/userStyle.css" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="main.begin" var="begin" />
<fmt:message bundle="${loc}" key="main.end" var="end" />
<fmt:message bundle="${loc}" key="main.return" var="returnToListCourse" />
<fmt:message bundle="${loc}" key="admin.addCourse.name" var="name" />
<fmt:message bundle="${loc}" key="admin.addCourse.description" var="description" />
<fmt:message bundle="${loc}" key="admin.saveEdit" var="saveEdit" />
</head>
<body bgcolor="#CAFF70">
	<form name="registrationForm" method="POST" action="${pageContext.request.contextPath}/Controller">
		<hr>
			<input type="hidden" name="command" value="editCourse" /><br /> ${name}:<br />
			<input type="text" name="courseName" value="${course.name}" /> <br /> ${description}:<br />
			<textarea name="description" rows="7" cols="30">${course.description}</textarea><br /> ${begin}: <br /> 
			<input type="text" name="beginCourse" value="${course.beginCourse}" /> <br />${end}: <br /> 
			<input type="text" name="endCourse" value="${course.endCourse}" /><br /> 
			<input type="hidden" name="courseId" value="${course.id}" />
			<input type="hidden" name="login" value="${user.login}" />
			<input	type="hidden" name="password" value="${user.password}" />
			 ${errorMessage} <br />
			<input type="submit" value="${saveEdit}" /> <br />
		<hr>
	</form>
	<a href="/facultative/jsp/admin/admin.jsp">${returnToListCourse}</a>
</body>
</html>