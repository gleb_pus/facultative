<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="user" scope="session" class="com.epam.entity.User" />
<html>
<head>
<title>Admin JSP</title>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/style/userStyle.css" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="main.begin" var="begin" />
<fmt:message bundle="${loc}" key="main.end" var="end" />
<fmt:message bundle="${loc}" key="admin.message1" var="message1" />
<fmt:message bundle="${loc}" key="admin.message2" var="message2" />
<fmt:message bundle="${loc}" key="admin.addCourse" var="addCourse" />
</head>
<body bgcolor="#CAFF70">
	<div class="yellowBlock">
		<h2>${message1}!</h2>
		<h3>${message2} </h3></br>
	</div>
	<div class="greenBlock">
		<c:forEach var="allcourse" items="${allCourses}">
				<c:url value="/Controller" var="url">
					<c:param name="courseId" value="${allcourse.id}" />
					<c:param name="command" value="adminCourseDescription" />
				</c:url>
			<h2><a href="${url}">${allcourse.name}</a></h2>
			<h5><c:out value="${begin}: ${allcourse.beginCourse}" /></h5>
			<hr>	
		</c:forEach>
		<h2><a href="/facultative/jsp/admin/adminAddCourse.jsp">${addCourse}</a></h2>
	</div>
</body>
</html>