<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="course" scope="request" class="com.epam.entity.Course" />
<html>
<head>
<title>Admin JSP</title>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/style/userStyle.css" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="teacher.students.message" var="message" />
<fmt:message bundle="${loc}" key="teacher.student.name" var="name" />
<fmt:message bundle="${loc}" key="teacher.student.surname" var="surname" />
<fmt:message bundle="${loc}" key="teacher.student.rate" var="rate" />
<fmt:message bundle="${loc}" key="teacher.review.review" var="review" />
<fmt:message bundle="${loc}" key="admin.deleteStudent" var="deleteStudentButton" />
<fmt:message bundle="${loc}" key="admin.addStudent" var="addStudent" />
<fmt:message bundle="${loc}" key="main.return" var="returnToListCourses" />
<fmt:message bundle="${loc}" key="empty.students" var="emptyStudents" />
</head>
<body bgcolor="#CAFF70">
	<div class="yellowBlock">
		<h2>${message} "${course.name}" </h2></br>
	</div>	
	<div class="greenBlock">
		<c:choose>
			<c:when test ="${not empty students}">
				<table class="tableStyle">
					<tr>
						<th>${name}</th>
						<th>${surname}</th>
						<th>${rate}</th>
						<th>${review}</th>
					</tr>
					<c:forEach var="student" items="${students}">
						<c:set var="rate" value="" />
						<c:set var="hasReview" value="false" />
						<c:set var="courseId" value="${course.id}" />
							<c:if test="${not empty student.reviews}">
								<c:forEach var="studentReview" items="${student.reviews}">
									<c:if test="${studentReview.courseId eq courseId}">
										<c:set var="rate" value="${studentReview.rate}" />
										<c:set var="review" value="${studentReview.review}" />
										<c:set var="hasReview" value="true" />
									</c:if>
								</c:forEach>
							</c:if>
							<c:choose>
								<c:when test="${hasReview eq 'true' }">
									<tr>
										<td><c:out value="${ student.firstName }" /></td>
										<td><c:out value="${ student.lastName}" /></td>
										<td><c:out value="${rate}" /></td>
										<td><c:out value="${review}" /></td>
										<td class="message">
											<c:url value="/Controller" var="url">
												<c:param name="courseId" value="${courseId}" />
												<c:param name="userId" value="${student.id}" />
												<c:param name="command" value="deleteStudent" />
											</c:url>
											<a href="${url}">${deleteStudentButton}</a></td>
									</tr>
								</c:when>
								<c:when test="${hasReview eq 'false' }">
									<tr>
										<td><c:out value="${ student.firstName }" /></td>
										<td><c:out value="${ student.lastName}" /></td>
										<td>-</td>
										<td>-</td>
										<td class="message">
											<c:url value="/Controller" var="url">
												<c:param name="courseId" value="${courseId}" />
												<c:param name="userId" value="${student.id}" />
												<c:param name="command" value="deleteStudent" />
											</c:url> 
											<a href="${url}">${deleteStudentButton}</a></td>
									</tr>
								</c:when>
							</c:choose>
					</c:forEach>
				</table></br>		
			</c:when>
			<c:when test ="${empty students}">
				<h2>${emptyStudents}</h2>	
			</c:when>
		</c:choose>
		<c:url value="/jsp/admin/adminAddStudent.jsp" var="url">
			<c:param name="courseId" value="${course.id}" />
		</c:url>
		<h2><a href="${url}">${addStudent}</a></h2>
		<h2><a href="/facultative/jsp/admin/admin.jsp">${returnToListCourses}</a></h2>	
	</div>
</body>
</html>