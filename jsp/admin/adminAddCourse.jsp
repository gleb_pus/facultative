<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>\
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="user" scope="session" class="com.epam.entity.User" />
<html>
<head>
<title>Admin JSP</title>
<link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/style/userStyle.css" />
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="main.begin" var="begin" />
<fmt:message bundle="${loc}" key="main.end" var="end" />
<fmt:message bundle="${loc}" key="main.return" var="returnToListCourse" />
<fmt:message bundle="${loc}" key="admin.addCourse.name" var="addName" />
<fmt:message bundle="${loc}" key="admin.addCourse.description" var="description" />
<fmt:message bundle="${loc}" key="admin.addCourse" var="addCourse" />
<fmt:message bundle="${loc}" key="admin.addCourse.teacher" var="teacher" />
</head>
<body bgcolor="#CAFF70">
	<form name="registrationForm" method="POST" action="${pageContext.request.contextPath}/Controller">
		<hr>
			<input type="hidden" name="command" value="addCourse" /><br /> ${addName}:<br />
			<input type="text" name="courseName" value="" /> <br /> ${description}:<br />
			<input type="text" name="description" value="" /> <br /> ${begin}: <br /> 
			<input type="text" name="beginCourse" value="" /> <br />${end}: <br /> 
			<input type="text" name="endCourse" value="" /><br />${teacher}: <br />	
			<input type="text" name="teacher" value="" /><br />	
			 ${errorMessage} <br />
			<input type="submit" value="${addCourse}" /> <br />
		<hr>
	</form>
	<a href="/facultative/jsp/admin/admin.jsp">${returnToListCourse}</a>
</body>
</html>