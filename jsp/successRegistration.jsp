<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="user" scope="session" class="com.epam.entity.User" />
<html>
<head>
<title>Registration JSP</title>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="successReg.message" var="message" />
<fmt:message bundle="${loc}" key="successReg.enterButton" var="enterButton" />
</head>
	<body bgcolor="#CAFF70">
	<div class="greenBlock">
		<h3>${user.firstName} ${user.lastName} ${message}</h3>
		<h2><a href="/facultative/jsp/student/studentCoursesList.jsp">${enterButton}</a></h2>
	</div>
	</body>
</html>