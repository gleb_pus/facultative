<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<title>Registration JSP</title>
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="reg.RegistrationButton" var="registrationButton" />
<fmt:message bundle="${loc}" key="reg.Login" var="login" />
<fmt:message bundle="${loc}" key="reg.Password" var="password" />
<fmt:message bundle="${loc}" key="reg.Name" var="name" />
<fmt:message bundle="${loc}" key="reg.SurName" var="surname" />
<fmt:message bundle="${loc}" key="reg.Email" var="email" />
</head>
<body bgcolor="#CAFF70">
	<form name="registrationForm" method="POST" action="${pageContext.request.contextPath}/Controller">
	<hr>
		<input type="hidden" name="command" value="registration" /><br /> ${login}:<br />
		<input type="text" name="login" value="" /> <br /> ${password}:<br />
		<input type="password" name="password" value="" /> <br /> ${email}: <br /> 
		<input type="text" name="email" value="" /><br /> ${name}: <br /> 
		<input type="text" name="firstName" value="" /><br /> ${surname}: <br /> 
		<input type="text" name="lastName" value="" /><br />
		${errorMessage} <br />
		<input type="submit" value="${registrationButton}" /> <br /> 
	<hr>
	</form>
</body>
</html>