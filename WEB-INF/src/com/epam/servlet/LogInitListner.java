package com.epam.servlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.PropertyConfigurator;

public class LogInitListner implements ServletContextListener {
	/**
	 * Initialize log4j when the application is being started
	 */
	@Override
	public void contextInitialized(ServletContextEvent event) {
		/* initialize log4j here */
		ServletContext servletContext = event.getServletContext();
		String log4jConfigLocation = servletContext.getInitParameter("log4jConfigLocation");
		String log4jFilename = servletContext.getRealPath(log4jConfigLocation);
		PropertyConfigurator.configure(log4jFilename);
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
	}
}
