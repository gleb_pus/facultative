package com.epam.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.epam.command.Command;
import com.epam.managers.CommandManager;
import com.epam.managers.ConfigurationManager;
import com.epam.managers.MessageManager;
/**
*
* @author Gleb Pus'
*/
public class Controller extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	private static Logger logger = Logger.getLogger(Controller.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	private void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String page;
		/* ComandManager is singleton */
		CommandManager commandManager = CommandManager.getInstance();
		
		logger.info("This is a logging statement from log4j");
		try {
			/* obtain command from request */
			Command command = commandManager.getCommand(request);
			/* Command pattern */
			page = command.execute(request);
			gotoPage(request, response, page);

		} catch (ServletException e) {
			logger.error("Servlet Exception " + e);
			/* generate error message */
			request.setAttribute("errorMessage",
					MessageManager.getInstance().getProperty(MessageManager.SERVLET_EXCEPTION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
			gotoPage(request, response, page);

		} catch (IOException e) {
			logger.error("IO Exception " + e);
			/* generate error message */
			request.setAttribute("errorMessage",
					MessageManager.getInstance().getProperty(MessageManager.IO_EXCEPTION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ERROR_PAGE_PATH);
			gotoPage(request, response, page);
		}
	}

	private void gotoPage(HttpServletRequest request, HttpServletResponse response, String page)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(page);
		dispatcher.forward(request, response);
	}
}
