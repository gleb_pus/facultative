package com.epam.command;

import javax.servlet.http.HttpServletRequest;

import com.epam.managers.ConfigurationManager;



public class EmptyCommand implements Command {
	/* This command called when parameter command empty */
	
	@Override
	public String execute(HttpServletRequest request) {

		String page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
		return page;
	}
}
