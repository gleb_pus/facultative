package com.epam.command.teacher;

import javax.servlet.http.HttpServletRequest;

import com.epam.command.Command;
import com.epam.constant.CourseConstants;
import com.epam.constant.UserConstants;
import com.epam.dao.CourseDAO;
import com.epam.dao.UserDAO;
import com.epam.entity.Course;
import com.epam.entity.Student;
import com.epam.managers.ConfigurationManager;

public class GoToReviewCommand implements Command { 
	/*
	 * By means of this command moves to the teacherReview.jsp with
	 * setting course in request
	 */
	
	private CourseDAO courseDAO = new CourseDAO();
	private UserDAO userDAO = new UserDAO();
	
	@Override
	public String execute(HttpServletRequest request){
	
		String page = null;
	
		int courseId = Integer.valueOf(request.getParameter(CourseConstants.PARAM_NAME_COURSE_ID));
		int studentId = Integer.valueOf(request.getParameter(UserConstants.ATTRIBUTE_USER_ID));
		
		/* get course by course id */
		Course course = courseDAO.findCourseById(courseId);
		
		/* get student by student id */
		Student student = userDAO.findStudentById(studentId);
		
		request.setAttribute(CourseConstants.PARAM_NAME_COURSE, course);
		request.setAttribute(UserConstants.PARAMETER_STUDENT, student);
		
		page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.TEACHER_REVIEW_PAGE_PATH);
		
		return page;
	}
}
