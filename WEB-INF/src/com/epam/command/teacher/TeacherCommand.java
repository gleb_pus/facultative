package com.epam.command.teacher;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import com.epam.command.Command;
import com.epam.constant.CourseConstants;
import com.epam.constant.UserConstants;
import com.epam.dao.CourseDAO;
import com.epam.entity.Course;
import com.epam.entity.Student;
import com.epam.managers.ConfigurationManager;

public class TeacherCommand implements Command { 
	/* By means of this command teacher see a list of their courses*/
	
	private CourseDAO courseDAO = new CourseDAO();
	
	@Override
	public String execute(HttpServletRequest request){
	
		String page = null;
	
		int courseId = Integer.valueOf(request.getParameter(CourseConstants.PARAM_NAME_COURSE_ID));
		
		/* get course by course id */
		Course course = courseDAO.findCourseById(courseId);
		
		/* get students of a concreate course*/		
		List<Student> students = courseDAO.getCourseStudentsById(courseId);  
		
		request.setAttribute(CourseConstants.PARAM_NAME_COURSE, course);
		request.setAttribute(UserConstants.ATTRIBUTE_STUDENTS, students);
		
		page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.TEACHER_STUDENTS_PAGE_PATH);
		return page;
	}
}
