package com.epam.command.teacher;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import com.epam.command.Command;
import com.epam.constant.CourseConstants;
import com.epam.constant.UserConstants;
import com.epam.dao.CourseDAO;
import com.epam.dao.UserDAO;
import com.epam.entity.Course;
import com.epam.entity.Student;
import com.epam.managers.ConfigurationManager;
import com.epam.managers.MessageManager;

public class EditReviewCommand implements Command {
	/* By means of this command teacher can edit a review of the student*/

	private CourseDAO courseDAO = new CourseDAO();
	private UserDAO userDAO = new UserDAO();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;

		int userId = Integer.valueOf(request.getParameter(UserConstants.ATTRIBUTE_USER_ID));
		int courseId = Integer.valueOf(request.getParameter(CourseConstants.PARAM_NAME_COURSE_ID));
		String rate = request.getParameter(UserConstants.PARAMETER_RATE);
		String review = request.getParameter(UserConstants.PARAMETER_REVIEW);

		if (rate.equals("") | review.equals("")) {
			/* check the input parameters for validity */
			request.setAttribute("errorMessage",
					MessageManager.getInstance().getProperty(MessageManager.REVIEW_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.TEACHER_EDIT_REVIEW_PAGE_PATH);
		} else {
			
			/* update review in database*/
			userDAO.updateReview(rate, review, userId, courseId);
			
			/* update students of a concreate course */
			List<Student> students = courseDAO.getCourseStudentsById(courseId);
			
			/* get course by course id */
			Course course = courseDAO.findCourseById(courseId);

			request.setAttribute(CourseConstants.PARAM_NAME_COURSE, course);
			request.setAttribute(UserConstants.ATTRIBUTE_STUDENTS, students);

			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.TEACHER_STUDENTS_PAGE_PATH);
		}
		return page;
	}
}
