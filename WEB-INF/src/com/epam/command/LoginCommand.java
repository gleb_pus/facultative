package com.epam.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.constant.CourseConstants;
import com.epam.constant.UserConstants;
import com.epam.dao.CourseDAO;
import com.epam.entity.Course;
import com.epam.entity.User;
import com.epam.entity.UserGroup;
import com.epam.managers.ConfigurationManager;
import com.epam.managers.MessageManager;
import com.epam.service.LoginService;

public class LoginCommand implements Command {
	/* By means of this command there is a login and password check and start page loading */

	private CourseDAO courseDAO = new CourseDAO();
	private LoginService loginService = new LoginService();

	/**
	* @param request
	* @return the start jsp page for concrete user
	*/
	public String execute(HttpServletRequest request) {

		String page = null;
		
		HttpSession session = request.getSession();

		/* get login and password from request */
		String login = request.getParameter(UserConstants.PARAMETER_LOGIN);
		String password = request.getParameter(UserConstants.PARAMETER_PASSWORD);

		/* get concrete user(student, teacher or admin) */
		User user = loginService.doLogin(login, password);
			
		if (user != null) {
			/* if this user register in system */
			List<Course> allCourses = courseDAO.getAllCourses();			
				session.setAttribute(UserConstants.ATTRIBUTE_USER, user);
				session.setAttribute(CourseConstants.PARAM_NAME_ALL_COURSES, allCourses);
				page = getLoginPageForUser(user);
		} else {
			/* if this user is not register */
			request.setAttribute("errorMessage", MessageManager.getInstance().getProperty(MessageManager.LOGIN_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.LOGIN_PAGE_PATH);
		}

		return page;
	}
	
	/* return page based on the type of user */
	protected String getLoginPageForUser(User user) {
		
		String page = null;
		UserGroup userGroup = user.getUserGroup();

		if (userGroup.getCode().equals(UserConstants.USERGROUP_STUDENT)) {
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.STUDENT_COURSES_LIST_PAGE_PATH);
		} else if (userGroup.getCode().equals(UserConstants.USERGROUP_TEACHER)) {
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.TEACHER_COURSES_PAGE_PATH);
		} else if (userGroup.getCode().equals(UserConstants.USERGROUP_ADMIN)) {
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_PAGE_PATH);
		}
		return page;
	}

	public CourseDAO getCourseDAO() {
		return courseDAO;
	}

	public void setCourseDAO(CourseDAO courseDAO) {
		this.courseDAO = courseDAO;
	}

	public LoginService getLoginService() {
		return loginService;
	}

	public void setLoginService(LoginService loginService) {
		this.loginService = loginService;
	}
}
