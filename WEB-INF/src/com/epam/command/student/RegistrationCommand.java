package com.epam.command.student;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.command.Command;
import com.epam.constant.CourseConstants;
import com.epam.constant.UserConstants;
import com.epam.dao.CourseDAO;
import com.epam.dao.LogicDAO;
import com.epam.dao.UserDAO;
import com.epam.entity.Course;
import com.epam.entity.User;
import com.epam.managers.ConfigurationManager;
import com.epam.managers.MessageManager;

public class RegistrationCommand implements Command {
	/* By means of this command user can register in the system */
	
	private UserDAO userDAO = new UserDAO();
	private CourseDAO courseDAO = new CourseDAO();
	private LogicDAO logicDAO = new LogicDAO();

	@Override
	public String execute(HttpServletRequest request) {
		
		String page = null;

		HttpSession session = request.getSession();

		String login = request.getParameter(UserConstants.PARAMETER_LOGIN);
		String password = request.getParameter(UserConstants.PARAMETER_PASSWORD);
		String firstname = request.getParameter(UserConstants.PARAMETER_FIRSTNAME);
		String lastname = request.getParameter(UserConstants.PARAMETER_LASTNAME);
		String email = request.getParameter(UserConstants.PARAMETER_EMAIL);

		if (login.equals("") | password.equals("") | firstname.equals("") | lastname.equals("") | email.equals("")) {
			/* check the input parameters for validity */
			request.setAttribute("errorMessage",
					MessageManager.getInstance().getProperty(MessageManager.REGISTRATION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.REGISTRATION_PAGE_PATH);
		} else if (logicDAO.checkUserByLogin(login)) {
			/* check whether there is entered login in the database */
			request.setAttribute("errorMessage",
					MessageManager.getInstance().getProperty(MessageManager.REGISTRATION_LOGIN_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.REGISTRATION_PAGE_PATH);
		} else if (logicDAO.checkUserByEmail(email)) {
			/* check whether there is entered email in the database */
			request.setAttribute("errorMessage",
					MessageManager.getInstance().getProperty(MessageManager.REGISTRATION_EMAIL_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.REGISTRATION_PAGE_PATH);
		} else {

			User user = new User();
			user.setLogin(login);
			user.setPassword(password);
			user.setFirstName(firstname);
			user.setLastName(lastname);
			user.setEmail(email);
			
			/* insert user in database */
			userDAO.createUser(user);

			User student = userDAO.findStudentByLoginPassword(login, password);		
			
			/* get all courses */
			List<Course> allCourses = courseDAO.getAllCourses();

			session.setAttribute(UserConstants.ATTRIBUTE_USER, student);
			session.setAttribute(CourseConstants.PARAM_NAME_ALL_COURSES, allCourses);

			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.SUCCESS_REGISTRATION_PAGE_PATH);
		}
		return page;
	}
}
