package com.epam.command.student;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.command.Command;
import com.epam.constant.CourseConstants;
import com.epam.constant.UserConstants;
import com.epam.dao.CourseDAO;
import com.epam.dao.LogicDAO;
import com.epam.entity.Course;
import com.epam.entity.User;
import com.epam.managers.ConfigurationManager;

public class CourseDescriptionCommand implements Command {
	/*
	 * By means of this command moves to the studentCourseDescription.jsp with
	 * setting course in request
	 */

	private CourseDAO courseDAO = new CourseDAO();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;
		
		int courseId = Integer.valueOf(request.getParameter(CourseConstants.PARAM_NAME_COURSE_ID));
		
		/* get course by course id */
		Course course = courseDAO.findCourseById(courseId);

		request.setAttribute(CourseConstants.PARAM_NAME_COURSE, course);
		
		page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.STUDENT_COURSE_DESCRIPTION_PAGE_PATH);
		return page;
	}
}
