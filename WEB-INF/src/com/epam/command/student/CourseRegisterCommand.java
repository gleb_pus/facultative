package com.epam.command.student;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.command.Command;
import com.epam.constant.CourseConstants;
import com.epam.constant.UserConstants;
import com.epam.dao.CourseDAO;
import com.epam.dao.LogicDAO;
import com.epam.entity.Course;
import com.epam.entity.User;
import com.epam.managers.ConfigurationManager;

public class CourseRegisterCommand implements Command {
	/* By means of this command user can register on a course */

	private LogicDAO logicDAO = new LogicDAO();
	private CourseDAO courseDAO = new CourseDAO();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;
		HttpSession session = request.getSession();

		User user = (User) session.getAttribute(UserConstants.ATTRIBUTE_USER);
		int userId = user.getId();
		int courseId = Integer.valueOf(request.getParameter(CourseConstants.PARAM_NAME_COURSE_ID));
		
		/* get course by course id */
		Course course = courseDAO.findCourseById(courseId);
		
		/*join of the user and the course*/
		logicDAO.joinUserCourse(courseId, userId);

		request.setAttribute(CourseConstants.PARAM_NAME_COURSE, course);
		
		page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.STUDENT_SUCCESS_REGISTER_PAGE_PATH);
		return page;
	}
}
