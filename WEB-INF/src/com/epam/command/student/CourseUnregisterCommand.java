package com.epam.command.student;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.command.Command;
import com.epam.constant.CourseConstants;
import com.epam.constant.UserConstants;
import com.epam.dao.CourseDAO;
import com.epam.dao.LogicDAO;
import com.epam.entity.Course;
import com.epam.entity.User;
import com.epam.managers.ConfigurationManager;

public class CourseUnregisterCommand implements Command {
	/* By means of this command user can unregister on a course */
	
	private LogicDAO logicDAO = new LogicDAO();
	private CourseDAO courseDAO = new CourseDAO();
	
	@Override
	public String execute(HttpServletRequest request) {
		
		HttpSession session = request.getSession();
			
		User user = (User) session.getAttribute(UserConstants.ATTRIBUTE_USER);
		int userId = user.getId();
		int courseId = Integer.valueOf(request.getParameter("courseId"));
		
		/* get course by course id */
		Course course = courseDAO.findCourseById(courseId);
				
		/*delete join of the user and the course*/
		logicDAO.deleteJoinUserCourse(userId, courseId);
		
		request.setAttribute(CourseConstants.PARAM_NAME_COURSE, course);
		
		String page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.STUDENT_SUCCESS_UNREGISTER_PAGE_PATH);
		return page;
	}
}
