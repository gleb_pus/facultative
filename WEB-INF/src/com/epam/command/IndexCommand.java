package com.epam.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.constant.UserConstants;
import com.epam.managers.ConfigurationManager;

public class IndexCommand  implements Command {
	/* By means of this command user can choose the locale and log in */
	
	@Override
	public String execute(HttpServletRequest request) {

		HttpSession session = request.getSession();
		
		String local = request.getParameter(UserConstants.PARAMETER_LOCAL);
		session.setAttribute(UserConstants.PARAMETER_LOCAL, local);
			
		String page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.INDEX_PAGE_PATH);
		return page;
	}
}


