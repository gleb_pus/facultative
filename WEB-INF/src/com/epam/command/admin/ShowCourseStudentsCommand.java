package com.epam.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.command.Command;
import com.epam.constant.CourseConstants;
import com.epam.constant.UserConstants;
import com.epam.dao.CourseDAO;
import com.epam.entity.Course;
import com.epam.entity.Student;
import com.epam.managers.ConfigurationManager;

public class ShowCourseStudentsCommand implements Command {
	/*
	 * By means of this command admin see students of the course and can create
	 * student and edit information about student
	 */
	private CourseDAO courseDAO = new CourseDAO();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;

		int courseId = Integer.valueOf(request.getParameter(CourseConstants.PARAM_NAME_COURSE_ID));

		/* update students of a concreate course */
		List<Student> students = courseDAO.getCourseStudentsById(courseId);
		
		/* get course by course id */
		Course course = courseDAO.findCourseById(courseId);

		request.setAttribute(CourseConstants.PARAM_NAME_COURSE, course);
		request.setAttribute(UserConstants.ATTRIBUTE_STUDENTS, students);

		page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_COURSE_STUDENTS_PAGE_PATH);
		return page;
	}
}