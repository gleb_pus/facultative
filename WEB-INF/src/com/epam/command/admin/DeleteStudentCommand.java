package com.epam.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.command.Command;
import com.epam.constant.CourseConstants;
import com.epam.constant.UserConstants;
import com.epam.dao.CourseDAO;
import com.epam.dao.LogicDAO;
import com.epam.entity.Course;
import com.epam.entity.Student;
import com.epam.managers.ConfigurationManager;

public class DeleteStudentCommand implements Command { 
	/* By means of this command admin can unregister student from course*/
	
	private CourseDAO courseDAO = new CourseDAO();
	private LogicDAO logicDAO = new LogicDAO();
	
	@Override
	public String execute(HttpServletRequest request){
	
		String page = null;
	
		int courseId = Integer.valueOf(request.getParameter(CourseConstants.PARAM_NAME_COURSE_ID));
		int userId = Integer.valueOf(request.getParameter(UserConstants.ATTRIBUTE_USER_ID));
		
		/*delete join of the user and the course*/
		logicDAO.deleteJoinUserCourse(userId, courseId);
				
		/* update students of a concreate course */
		List<Student> students = courseDAO.getCourseStudentsById(courseId);
		
		/* get course by course id */
		Course course = courseDAO.findCourseById(courseId);
		
		request.setAttribute(CourseConstants.PARAM_NAME_COURSE, course);
		request.setAttribute(UserConstants.ATTRIBUTE_STUDENTS, students);
		
		page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_COURSE_STUDENTS_PAGE_PATH);
		return page;
	}
}
