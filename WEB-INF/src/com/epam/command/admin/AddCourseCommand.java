package com.epam.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.command.Command;
import com.epam.constant.CourseConstants;
import com.epam.constant.UserConstants;
import com.epam.dao.CourseDAO;
import com.epam.dao.LogicDAO;
import com.epam.dao.UserDAO;
import com.epam.entity.Course;
import com.epam.entity.Teacher;
import com.epam.managers.ConfigurationManager;
import com.epam.managers.MessageManager;

public class AddCourseCommand implements Command {
	/* By means of this command admin can add course in database*/
	
	private CourseDAO courseDAO = new CourseDAO();
	private UserDAO userDAO = new UserDAO();
	private LogicDAO logicDAO = new LogicDAO();

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		
		HttpSession session = request.getSession();
		
		String courseName = request.getParameter(CourseConstants.PARAM_NAME_NAME);
		String description = request.getParameter(CourseConstants.PARAM_NAME_DESCRIPTION);
		String beginCourse = request.getParameter(CourseConstants.PARAM_NAME_BEGIN_COURSE);
		String endCourse = request.getParameter(CourseConstants.PARAM_NAME_END_COURSE);
		String teacherLastName = request.getParameter(UserConstants.USERGROUP_TEACHER);
		
		/* get teacher by surname */
		Teacher teacher = userDAO.findTeacherByLastName(teacherLastName);

		if (courseName.equals("") | description.equals("") | beginCourse.equals("") | endCourse.equals("")) {
			/* check the input parameters for validity */
			request.setAttribute("errorMessage",
					MessageManager.getInstance().getProperty(MessageManager.COURSE_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_ADD_COURSE_PAGE_PATH);
		} else if( teacher == null) {
			/* check whether there is a teacher in the database */
			request.setAttribute("errorMessage",
					MessageManager.getInstance().getProperty(MessageManager.COURSE_TEACHER_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_ADD_COURSE_PAGE_PATH);
		} else{
						
			Course course = new Course();
			course.setName(courseName);
			course.setDescription(description);
			course.setBeginCourse(beginCourse);
			course.setEndCourse(endCourse);
			course.setTeacher(teacher);
			
			/* add course in database */
			courseDAO.createCourse(course);
			
			/* get id created course */
			int courseId = courseDAO.findCourseIdByName(courseName);
			
			/* join teacher with course */
			logicDAO.joinUserCourse(courseId, teacher.getId());
			
			/* update all courses */ 
			List<Course> allCourses = courseDAO.getAllCourses();
	
			session.setAttribute(CourseConstants.PARAM_NAME_ALL_COURSES, allCourses);
			
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_PAGE_PATH);
		}
		return page;

	}

}
