package com.epam.command.admin;

import javax.servlet.http.HttpServletRequest;

import com.epam.command.Command;
import com.epam.constant.CourseConstants;
import com.epam.dao.CourseDAO;
import com.epam.entity.Course;
import com.epam.managers.ConfigurationManager;

public class GoToAdminEditCourseCommand implements Command { 
	/*
	 * By means of this command moves to the adminEditCourse.jsp with
	 * setting course in request
	 */
	private CourseDAO courseDAO = new CourseDAO();
	
	@Override
	public String execute(HttpServletRequest request){
	
		String page = null;
	
		int courseId = Integer.valueOf(request.getParameter(CourseConstants.PARAM_NAME_COURSE_ID));
		
		/* get course by course id */
		Course course = courseDAO.findCourseById(courseId);
		
		request.setAttribute(CourseConstants.PARAM_NAME_COURSE, course);
		
		page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_EDIT_COURSE_PAGE_PATH);
		
		return page;
	}
}
