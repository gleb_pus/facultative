package com.epam.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.epam.command.Command;
import com.epam.constant.CourseConstants;
import com.epam.constant.UserConstants;
import com.epam.dao.CourseDAO;
import com.epam.dao.LogicDAO;
import com.epam.dao.UserDAO;
import com.epam.entity.Course;
import com.epam.entity.Student;
import com.epam.entity.User;
import com.epam.managers.ConfigurationManager;
import com.epam.managers.MessageManager;

public class AddStudentCommand implements Command {
	/*
	 * By means of this command admin can create student and register him on
	 * course
	 */
	private CourseDAO courseDAO = new CourseDAO();
	private LogicDAO logicDAO = new LogicDAO();
	private UserDAO userDAO = new UserDAO();

	@Override
	public String execute(HttpServletRequest request) {

		String page = null;

		int courseId = Integer.valueOf(request.getParameter(CourseConstants.PARAM_NAME_COURSE_ID));
		String login = request.getParameter(UserConstants.PARAMETER_LOGIN);
		String password = request.getParameter(UserConstants.PARAMETER_PASSWORD);
		String firstname = request.getParameter(UserConstants.PARAMETER_FIRSTNAME);
		String lastname = request.getParameter(UserConstants.PARAMETER_LASTNAME);
		String email = request.getParameter(UserConstants.PARAMETER_EMAIL);

		if (login.equals("") | password.equals("") | firstname.equals("") | lastname.equals("") | email.equals("")) {
			/* check the input parameters for validity */
			request.setAttribute("errorMessage",
					MessageManager.getInstance().getProperty(MessageManager.REGISTRATION_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_ADD_STUDENT_PAGE_PATH);
		} else if (logicDAO.checkUserByLogin(login)) {
			/* check whether there is entered login in the database */
			request.setAttribute("errorMessage",
					MessageManager.getInstance().getProperty(MessageManager.REGISTRATION_LOGIN_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_ADD_STUDENT_PAGE_PATH);
		} else if (logicDAO.checkUserByEmail(email)) {
			/* check whether there is entered email in the database */
			request.setAttribute("errorMessage",
					MessageManager.getInstance().getProperty(MessageManager.REGISTRATION_EMAIL_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_ADD_STUDENT_PAGE_PATH);
		} else {

			User user = new User();
			user.setLogin(login);
			user.setPassword(password);
			user.setFirstName(firstname);
			user.setLastName(lastname);
			user.setEmail(email);
			
			/* insert student in database */
			userDAO.createUser(user);

			/* get id created student */
			int userId = userDAO.findUserIdByLogin(login);

			/*join of the student and the course*/
			logicDAO.joinUserCourse(courseId, userId);

			/* update students of a concreate course */
			List<Student> students = courseDAO.getCourseStudentsById(courseId);
			
			/* get course by course id */
			Course course = courseDAO.findCourseById(courseId);

			request.setAttribute(CourseConstants.PARAM_NAME_COURSE, course);
			request.setAttribute(UserConstants.ATTRIBUTE_STUDENTS, students);

			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_COURSE_STUDENTS_PAGE_PATH);
		}
		return page;
	}
}
