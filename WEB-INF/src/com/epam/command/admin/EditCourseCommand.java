package com.epam.command.admin;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.command.Command;
import com.epam.constant.CourseConstants;
import com.epam.dao.CourseDAO;
import com.epam.entity.Course;
import com.epam.managers.ConfigurationManager;
import com.epam.managers.MessageManager;

public class EditCourseCommand implements Command {
	/* By means of this command admin can edit information about course*/

	private CourseDAO courseDAO = new CourseDAO();

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		
		HttpSession session = request.getSession();

		String courseName = request.getParameter(CourseConstants.PARAM_NAME_NAME);
		String description = request.getParameter(CourseConstants.PARAM_NAME_DESCRIPTION);
		String beginCourse = request.getParameter(CourseConstants.PARAM_NAME_BEGIN_COURSE);
		String endCourse = request.getParameter(CourseConstants.PARAM_NAME_END_COURSE);
		int courseId = Integer.valueOf(request.getParameter(CourseConstants.PARAM_NAME_COURSE_ID));

		if (courseName.equals("") | description.equals("") | beginCourse.equals("") | endCourse.equals("")) {
			/* check the input parameters for validity */
			request.setAttribute("errorMessage",
					MessageManager.getInstance().getProperty(MessageManager.COURSE_ERROR_MESSAGE));
			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_EDIT_COURSE_PAGE_PATH);
		} else {

			/* update information about course */
			courseDAO.updateCourse(courseName, description, beginCourse, endCourse, courseId);
			
			/* update all courses */ 
			List<Course> allCourses = courseDAO.getAllCourses();

			session.setAttribute(CourseConstants.PARAM_NAME_ALL_COURSES, allCourses);

			page = ConfigurationManager.getInstance().getProperty(ConfigurationManager.ADMIN_PAGE_PATH);
		}

		return page;
	}
}
