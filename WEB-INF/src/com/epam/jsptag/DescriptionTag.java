package com.epam.jsptag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

public class DescriptionTag extends TagSupport {

	/* course description */
	private String description;

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public int doStartTag() throws JspException {

		try {
			/*display course description on jsp*/
			pageContext.getOut().write(description);

		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}
}
