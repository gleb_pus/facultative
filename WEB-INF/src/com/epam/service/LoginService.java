package com.epam.service;

import com.epam.constant.UserConstants;
import com.epam.dao.UserDAO;
import com.epam.entity.User;
import com.epam.entity.UserGroup;

public class LoginService {
	
	private UserDAO userDAO = new UserDAO();	
	/**
	* @return user by login and password
	*/
	public User doLogin(String login, String password) {
		
		User user = null;
		
		/*get userGroup by login and password*/
		UserGroup userGroup = userDAO.findUserGroupByLoginPassword(login, password); 
		
		if (userGroup != null) {
			user = getUser(login, password, userGroup);
		}
		return user;
	}
	/**
	* @return teacher, student or admin
	*/
	public User getUser(String login, String password, UserGroup userGroup) {
		User user = null;
		
		/*check userGroup with user userGroup*/
		if (userGroup.getCode().equals(UserConstants.USERGROUP_STUDENT)) { 
			user = userDAO.findStudentByLoginPassword(login,password);
			return user;
		} else if (userGroup.getCode().equals(UserConstants.USERGROUP_TEACHER)) {
			user = userDAO.findTeacherByLoginPassword(login,password);
			return user;
		} else if (userGroup.getCode().equals(UserConstants.USERGROUP_ADMIN)) {
			user = userDAO.findAdminByLogin(login,password);
			return user;
		}
		return user;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}
}
