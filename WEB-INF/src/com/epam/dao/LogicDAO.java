package com.epam.dao;

import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.epam.dbconnector.DBConnector;


public class LogicDAO extends AbstractDAO {
	
	private static Logger logger = Logger.getLogger(LogicDAO.class);

	public final static String SQL_DELETE_JOIN_USER_COURSE = "DELETE "
			+ "FROM courseuser "
			+ "WHERE courseid = PARAM1 AND userid = PARAM2";
	
	public final static String SQL_JOIN_USER_AND_COURSE = "INSERT "
			+ "INTO courseuser( courseid, userid) VALUES(PARAM1,PARAM2)";
	
	public final static String CHECK_USER_BY_LOGIN = "SELECT password "
			+ "FROM user "
			+ "WHERE login = 'PARAM'";
	
	public final static String CHECK_USER_BY_EMAIL = "SELECT login "
			+ "FROM user "
			+ "WHERE email = 'PARAM'";
	/**
	* Delete user Id course Id from usercourse table
	*/
	public void deleteJoinUserCourse(int userId, int courseId) {

		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			statement.executeUpdate(SQL_DELETE_JOIN_USER_COURSE.replace("PARAM1", String.valueOf(courseId))
					.replace("PARAM2", String.valueOf(userId)));

		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
	}
	/**
	* Insert into courseuser table
	*/
	public void joinUserCourse(int courseId, int userId) {

		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			statement.executeUpdate(SQL_JOIN_USER_AND_COURSE.replace("PARAM1", String.valueOf(courseId))
					.replace("PARAM2", String.valueOf(userId)));

		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
	}
	/**
	* @return true if the user with such login exist 
	*/
	public boolean checkUserByLogin(String login) {
		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement.executeQuery(CHECK_USER_BY_LOGIN.replace("PARAM", login));
			if (resultSet.next()) {
				return true;
			}
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
		return false;
	}
	/**
	* @return false if the user with such email exist 
	*/
	public boolean checkUserByEmail(String email) {
		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement.executeQuery(CHECK_USER_BY_EMAIL.replace("PARAM", email));
			if (resultSet.next()) {
				return true;
			}
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
		return false;
	}
}
