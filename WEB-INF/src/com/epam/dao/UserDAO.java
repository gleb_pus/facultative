package com.epam.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.dbconnector.DBConnector;
import com.epam.entity.Course;
import com.epam.entity.Review;
import com.epam.entity.Student;
import com.epam.entity.Teacher;
import com.epam.entity.User;
import com.epam.entity.UserGroup;


public class UserDAO extends AbstractDAO {

	private static Logger logger = Logger.getLogger(UserDAO.class);

	private final static String SQL_FIND_USER_BY_ID = "SELECT * "
			+ "FROM user "
			+ "WHERE id = PARAM ";
	
	private final static String SQL_ADD_USER = "INSERT "
			+ "INTO user( login, password, email, firstname, lastname) VALUES(?,?,?,?,?)";
	
	private final static String SQL_FIND_USERGROUP_BY_LOGIN = "SELECT usergroup.code , usergroup.name "
			+ "FROM usergroup "
			+ "JOIN user ON  usergroup.id = user.usergroup "
			+ "WHERE user.login = 'PARAM1' AND user.password ='PARAM2'";
	
	private final static String SQL_FIND_STUDENT_BY_LOGIN = "SELECT * "
			+ "FROM user "
			+ "JOIN usergroup ON user.usergroup = usergroup.id"
			+ " WHERE user.login = 'PARAM1' AND user.password ='PARAM2'";
	
	private final static String SQL_FIND_STUDENT_BY_ID = "SELECT * FROM user WHERE id = PARAM";
	
	private final static String SQL_FIND_TEACHER_BY_LOGIN = "SELECT * "
			+ "FROM user "
			+ "JOIN usergroup ON user.usergroup = usergroup.id "
			+ "WHERE user.login = 'PARAM1' AND user.password ='PARAM2'";
	
	private final static String SQL_FIND_TEACHER_BY_ID = "SELECT user.id,user.firstname, user.lastname "
			+ "FROM user "
			+ "JOIN course ON course.teacher = user.id "
			+ "WHERE course.id =PARAM";
	
	private final static String SQL_FIND_STUDENT_RIVIEWS = "SELECT review.id, review.review, review.rate, review.courseid, review.userid "
			+ "FROM review "
			+ "JOIN user ON review.userid = user.id "
			+ "WHERE user.id = PARAM";
	
	private final static String SQL_FIND_TEACHER_BY_LASTNAME = "SELECT user.id,user.firstname, user.lastname, user.email "
			+ "FROM user "
			+ "JOIN usergroup ON user.usergroup = usergroup.id "
			+ "WHERE lastname = 'PARAM' AND usergroup.id=2";
	
	private static final String SQL_SELECT_ID_USER_BY_LOGIN = "SELECT id "
			+ "FROM facultative.user "
			+ "WHERE login = 'PARAM' ";
	
	private static final String SQL_INSERT_REVIEW = "INSERT "
			+ "INTO review(review, rate, courseid, userid) VALUES(?,?,?,?) ";
	
	private static final String SQL_UPDATE_REVIEW = "UPDATE review "
			+ "SET review=?, rate=?, courseid=?,userid=? "
			+ "WHERE userid = PARAM1 AND courseid = PARAM2 ";
	
	/**
	* @return userGroup by login and password
	*/
	public UserGroup findUserGroupByLoginPassword(String login, String password) {
		UserGroup userGroup = null;

		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement
					.executeQuery(SQL_FIND_USERGROUP_BY_LOGIN.replace("PARAM1", login).replace("PARAM2", password));
			if (resultSet.next()) {
				userGroup = new UserGroup();
				userGroup.setCode(resultSet.getString("usergroup.code"));
				userGroup.setName(resultSet.getString("usergroup.name"));
				return userGroup;
			}
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
		return userGroup;
	}
	/**
	* @return list student review by user Id
	*/
	public List<Review> getStudentsReviews(int userId) {
		List<Review> reviews = new ArrayList<Review>();
		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement.executeQuery(SQL_FIND_STUDENT_RIVIEWS.replace("PARAM", String.valueOf(userId)));

			while (resultSet.next()) {

				Review review = new Review();

				review.setId(resultSet.getInt("id"));
				review.setReview(resultSet.getString("review"));
				review.setRate(resultSet.getInt("rate"));
				review.setUserId(resultSet.getInt("userid"));
				review.setCourseId(resultSet.getInt("courseid"));

				reviews.add(review);
			}
			return reviews;
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);

		} finally {
			this.close();
		}
		return reviews;
	}
	/**
	* @return student by login and password
	*/
	public Student findStudentByLoginPassword(String login, String password) {
		Student student = null;
		ResultSet resultSet = null;
		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();

			resultSet = statement
					.executeQuery(SQL_FIND_STUDENT_BY_LOGIN.replace("PARAM1", login).replace("PARAM2", password));
			if (resultSet.next()) {
				student = new Student();
				UserGroup userGroup = new UserGroup();
				CourseDAO courseDAO = new CourseDAO();
				int userId = resultSet.getInt("user.id");
				List<Course> courses = courseDAO.findUserCourses(userId); 
				List<Review> reviews = getStudentsReviews(userId);

				userGroup.setId(resultSet.getInt("usergroup.id"));
				userGroup.setName(resultSet.getString("usergroup.name"));
				userGroup.setCode(resultSet.getString("usergroup.code"));

				student.setId(userId);
				student.setLogin(resultSet.getString("user.login"));
				student.setPassword(resultSet.getString("user.password"));
				student.setFirstName(resultSet.getString("user.firstname"));
				student.setLastName(resultSet.getString("user.lastname"));
				student.setEmail(resultSet.getString("user.email"));
				student.setCourses(courses);
				student.setUserGroup(userGroup);
				student.setReviews(reviews);
			}
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					logger.error("SQLException " + e);
				}
			}
			connector.close();
		}
		return student;
	}
	/**
	* @return student by id
	*/
	public Student findStudentById(int userId) {
		Student student = null;
		ResultSet resultSet = null;
		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();

			resultSet = statement
					.executeQuery(SQL_FIND_STUDENT_BY_ID.replace("PARAM", String.valueOf(userId)));
			if (resultSet.next()) {
				student = new Student();
				CourseDAO courseDAO = new CourseDAO();
				
				List<Course> courses = courseDAO.findUserCourses(userId); 
				List<Review> reviews = getStudentsReviews(userId);

				student.setId(userId);
				student.setLogin(resultSet.getString("user.login"));
				student.setPassword(resultSet.getString("user.password"));
				student.setFirstName(resultSet.getString("user.firstname"));
				student.setLastName(resultSet.getString("user.lastname"));
				student.setEmail(resultSet.getString("user.email"));
				student.setCourses(courses);
				student.setReviews(reviews);
			}
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					logger.error("SQLException " + e);
				}
			}
			connector.close();
		}
		return student;
	}
	/**
	* @return teacher review by login and password
	*/
	public Teacher findTeacherByLoginPassword(String login, String password) {
		Teacher teacher = null;
		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement
					.executeQuery(SQL_FIND_TEACHER_BY_LOGIN.replace("PARAM1", login).replace("PARAM2", password));
			if (resultSet.next()) {
				teacher = new Teacher();
				UserGroup userGroup = new UserGroup();

				CourseDAO courseDAO = new CourseDAO();
				int userId = resultSet.getInt("id");
				List<Course> courses = courseDAO.findUserCourses(userId);

				userGroup.setId(resultSet.getInt("usergroup.id"));
				userGroup.setName(resultSet.getString("usergroup.name"));
				userGroup.setCode(resultSet.getString("usergroup.code"));

				teacher.setId(resultSet.getInt("user.id"));
				teacher.setLogin(resultSet.getString("user.login"));
				teacher.setPassword(resultSet.getString("user.password"));
				teacher.setFirstName(resultSet.getString("user.firstname"));
				teacher.setLastName(resultSet.getString("user.lastname"));
				teacher.setEmail(resultSet.getString("user.email"));
				teacher.setUserGroup(userGroup);
				teacher.setCourses(courses);
			}
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
		return teacher;
	}
	
	/**
	* @return admin review by login and password
	*/
	public User findAdminByLogin(String login, String password) {
		User admin = null;
		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement
					.executeQuery(SQL_FIND_TEACHER_BY_LOGIN.replace("PARAM1", login).replace("PARAM2", password));
			if (resultSet.next()) {
				admin = new User();
				UserGroup userGroup = new UserGroup();

				userGroup.setId(resultSet.getInt("usergroup.id"));
				userGroup.setName(resultSet.getString("usergroup.name"));
				userGroup.setCode(resultSet.getString("usergroup.code"));

				admin.setId(resultSet.getInt("id"));
				admin.setLogin(resultSet.getString("login"));
				admin.setPassword(resultSet.getString("password"));
				admin.setFirstName(resultSet.getString("firstname"));
				admin.setLastName(resultSet.getString("lastname"));
				admin.setEmail(resultSet.getString("email"));
				admin.setUserGroup(userGroup);

			}
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
		return admin;
	}
	/**
	* @return teacher of the course by course Id
	*/
	public Teacher getTeacherByCourseId(int courseId) {
		Teacher teacher = null;

		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement.executeQuery(SQL_FIND_TEACHER_BY_ID.replace("PARAM", String.valueOf(courseId)));

			if (resultSet.next()) {
				teacher = new Teacher();
				teacher.setId(resultSet.getInt("id"));
				teacher.setFirstName(resultSet.getString("firstname"));
				teacher.setLastName(resultSet.getString("lastname"));
			}
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
		return teacher;

	}
	/**
	* @return user of by user Id
	*/
	public User getUserById(int userId) {
		User user = new User();

		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement.executeQuery(UserDAO.SQL_FIND_USER_BY_ID.replace("PARAM", String.valueOf(userId)));

			if (resultSet.next()) {
				user.setLogin(resultSet.getString("login"));
				user.setPassword(resultSet.getString("password"));
				user.setFirstName(resultSet.getString("firstname"));
				user.setLastName(resultSet.getString("lastname"));
				user.setEmail(resultSet.getString("email"));
			}
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
		return user;
	}
	/**
	* Insert into table user
	*/
	public void createUser(User user) {

		try {
			this.connector = new DBConnector();
			PreparedStatement ps = connector.getPreparedStatement(SQL_ADD_USER);

			ps.setString(1, user.getLogin());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getEmail());
			ps.setString(4, user.getFirstName());
			ps.setString(5, user.getLastName());
			ps.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
	}
	/**
	* @return teacher by surname
	*/
	public Teacher findTeacherByLastName(String teacherLastName){
		Teacher teacher = null;
		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement.executeQuery(UserDAO.SQL_FIND_TEACHER_BY_LASTNAME.replace("PARAM", teacherLastName));

			if (resultSet.next()) {
				teacher = new Teacher();
				teacher.setId(resultSet.getInt("id"));
				teacher.setFirstName(resultSet.getString("firstname"));
				teacher.setLastName(resultSet.getString("lastname"));
				teacher.setEmail(resultSet.getString("email"));
			}
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
		return teacher;		
	}
	/**
	* @return user Id by login
	*/
	public Integer findUserIdByLogin(String login) {
		Integer id = null;

		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement.executeQuery(SQL_SELECT_ID_USER_BY_LOGIN.replace("PARAM", login));

			if (resultSet.next()) {
				id = resultSet.getInt("id");
			}
			return id;

		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}

		return id;
	}
	/**
	* Insert into review table
	*/
	public void createReview(String rate, String review, int userId, int courseId) {
		try {
			this.connector = new DBConnector();
			PreparedStatement ps = connector.getPreparedStatement(SQL_INSERT_REVIEW);

			ps.setString(1, review);
			ps.setString(2, rate);
			ps.setInt(3, courseId);
			ps.setInt(4, userId);
			ps.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
	}
	/**
	* Update review
	*/
	public void updateReview(String rate, String review, int userId, int courseId) {
		try {
			this.connector = new DBConnector();
			PreparedStatement ps = connector.getPreparedStatement(SQL_UPDATE_REVIEW
					.replace("PARAM1", String.valueOf(userId)).replace("PARAM2", String.valueOf(courseId)));

			ps.setString(1, review);
			ps.setString(2, rate);
			ps.setInt(3, courseId);
			ps.setInt(4, userId);
			ps.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
	}
}
