package com.epam.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.epam.dbconnector.DBConnector;
import com.epam.entity.Course;
import com.epam.entity.Review;
import com.epam.entity.Student;
import com.epam.entity.Teacher;


public class CourseDAO extends AbstractDAO {
	
	private static Logger logger = Logger.getLogger(CourseDAO.class);

	private static final String SQL_SELECT_ALL_COURSES = "SELECT * "
			+ "FROM course";
	
	private static final String SQL_ADD_COURSE = "INSERT "
			+ "INTO course( name, description,teacher, beginCourse, endCourse) VALUES(?,?,?,?,?)";
	
	private static final String SQL_DELETE_COURSE = "DELETE "
			+ "FROM course "
			+ "WHERE id = PARAM";
	
	private final static String SQL_SELECT_USER_COURSES = "SELECT course.id, course.name, course.description, course.teacher, course.beginCourse, course.endCourse "
			+ "FROM course "
			+ "JOIN courseuser ON course.id = courseuser.courseid "
			+ "JOIN user ON courseuser.userid = user.id "
			+ "WHERE user.id = PARAM"; 
	
	private static final String SQL_SELECT_COURSE_STUDENTS_BY_ID = "SELECT user.id, user.login, user.password, user.firstname, user.lastname, user.email "
			+ "FROM user "
			+ "JOIN courseuser ON user.id = courseuser.userid "
			+ "WHERE courseuser.courseid = PARAM AND user.usergroup = 1"; 
	
	private static final String SQL_UPDATE_COURSE = "UPDATE course "
			+ "SET name=?, description=?, beginCourse=?,endCourse=? "
			+ "WHERE id = PARAM";
	
	private final static String FIND_COURSE_ID_BY_NAME = "SELECT id "
			+ "FROM course "
			+ "WHERE name = 'PARAM'";
	private final static String FIND_COURSE_BY_ID = "SELECT * "
			+ "FROM course "
			+ "WHERE id = PARAM";
	/**
	* @return course by course id
	*/
	public Course findCourseById(int courseId){
		
		ResultSet resultSet = null;
		Course course = null;
		
		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement.executeQuery(FIND_COURSE_BY_ID.replace("PARAM", String.valueOf(courseId)));
			while (resultSet.next()) {
				course = new Course();
				UserDAO userDAO = new UserDAO();
				List<Student> students = getCourseStudentsById(courseId);
				Teacher teacher = userDAO.getTeacherByCourseId(courseId);

				course.setId(courseId);
				course.setName(resultSet.getString("name"));
				course.setDescription(resultSet.getString("description"));
				course.setTeacher(teacher);
				course.setBeginCourse(resultSet.getString("beginCourse"));
				course.setEndCourse(resultSet.getString("endCourse"));
				course.setStudents(students);			
			return course;
			}
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					logger.error("SQL exception, request or table failed: " + e);
				}
			}
			connector.close();
		}
		return course;
	}
	/**
	* @return list of courses concrete student
	*/
	public List<Course> findUserCourses(int userId) {

		ResultSet resultSet = null;
		List<Course> courses = new ArrayList<Course>();

		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement.executeQuery(SQL_SELECT_USER_COURSES.replace("PARAM", String.valueOf(userId)));
			UserDAO userDAO = new UserDAO();
			while (resultSet.next()) {
				Course course = new Course();
				int courseId = resultSet.getInt("id");
				List<Student> students = getCourseStudentsById(courseId);
				Teacher teacher = userDAO.getTeacherByCourseId(courseId);

				course.setId(courseId);
				course.setName(resultSet.getString("name"));
				course.setDescription(resultSet.getString("description"));
				course.setTeacher(teacher);
				course.setBeginCourse(resultSet.getString("beginCourse"));
				course.setEndCourse(resultSet.getString("endCourse"));
				course.setStudents(students);

				courses.add(course);
			}
			return courses;
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			if (resultSet != null) {
				try {
					resultSet.close();
				} catch (SQLException e) {
					logger.error("SQL exception, request or table failed: " + e);
				}
			}
			connector.close();
		}
		return courses;
	}
	/**
	* @return list of all courses
	*/
	public List<Course> getAllCourses() {

		List<Course> courses = new ArrayList<Course>();
		UserDAO userDAO = new UserDAO();

		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement.executeQuery(SQL_SELECT_ALL_COURSES);

			while (resultSet.next()) {
				Course course = new Course();
				int courseId = resultSet.getInt("id");

				Teacher teacher = userDAO.getTeacherByCourseId(courseId);

				course.setId(courseId);
				course.setName(resultSet.getString("name"));
				course.setDescription(resultSet.getString("description"));
				course.setBeginCourse(resultSet.getString("beginCourse"));
				course.setEndCourse(resultSet.getString("endCourse"));
				course.setTeacher(teacher);

				courses.add(course);
			}
			return courses;
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
		return courses;
	}
	/**
	* Insert into course table
	*/
	public void createCourse(Course course) {
		
		try {
			this.connector = new DBConnector();
			PreparedStatement ps = connector.getPreparedStatement(SQL_ADD_COURSE);

			ps.setString(1, course.getName());
			ps.setString(2, course.getDescription());
			ps.setInt(3, course.getTeacher().getId());
			ps.setString(4, course.getBeginCourse());
			ps.setString(5, course.getEndCourse());

			ps.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
	}
	/**
	* Delete course by course Id
	*/
	public void deleteCourse(int courseId) {

		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			statement.executeUpdate(SQL_DELETE_COURSE.replace("PARAM", String.valueOf(courseId)));
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
	}
	/**
	* @return list of student concrete course by course Id
	*/
	public List<Student> getCourseStudentsById(int courseId) {
		
		List<Student> students = new ArrayList<Student>();

		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement
					.executeQuery(SQL_SELECT_COURSE_STUDENTS_BY_ID.replace("PARAM", String.valueOf(courseId)));

			while (resultSet.next()) {
				Student student = new Student();
				UserDAO userDAO = new UserDAO();
				int userId = resultSet.getInt("id");
				List<Review> reviews = userDAO.getStudentsReviews(userId);

				student.setId(resultSet.getInt("id"));
				student.setLogin(resultSet.getString("login"));
				student.setPassword(resultSet.getString("password"));
				student.setFirstName(resultSet.getString("firstname"));
				student.setLastName(resultSet.getString("lastname"));
				student.setEmail(resultSet.getString("email"));
				student.setReviews(reviews);

				students.add(student);
			}
			return students;
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
		return students;
	}
	/**
	* Update course
	*/
	public void updateCourse(String name, String description, String beginCourse, String endCourse, int courseId) {
		
		try {
			this.connector = new DBConnector();
			PreparedStatement ps = connector
					.getPreparedStatement(SQL_UPDATE_COURSE.replace("PARAM", String.valueOf(courseId)));

			ps.setString(1, name);
			ps.setString(2, description);
			ps.setString(3, beginCourse);
			ps.setString(4, endCourse);
			ps.executeUpdate();
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
	}
	/**
	* @return course by course Id
	*/
	public int findCourseIdByName(String courseName){
		
		Integer courseId = null;
		
		try {
			this.connector = new DBConnector();
			Statement statement = connector.getStatement();
			resultSet = statement.executeQuery(FIND_COURSE_ID_BY_NAME.replace("PARAM", courseName));

			if (resultSet.next()) {
				courseId = resultSet.getInt("id");
			}
		} catch (SQLException e) {
			logger.error("SQL exception, request or table failed: " + e);
		} finally {
			this.close();
		}
		return courseId;		
	}
}
