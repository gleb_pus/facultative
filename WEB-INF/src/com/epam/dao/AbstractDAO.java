package com.epam.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.epam.dbconnector.DBConnector;



public abstract class AbstractDAO {

	private static Logger logger = Logger.getLogger(AbstractDAO.class);
	/**
	 * This object stores data obtained from the database
	 */
	protected ResultSet resultSet;
	/**
	 * This object stores connector from connection pool
	 */
	protected DBConnector connector;

	public void close() {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				logger.error("SQLException " + e);
			}
		}
		connector.close();
	}

}
