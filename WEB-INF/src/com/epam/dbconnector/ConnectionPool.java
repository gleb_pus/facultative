package com.epam.dbconnector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import com.epam.managers.ConfigurationManager;

public class ConnectionPool {
	
	private static final Logger logger = Logger.getLogger(ConnectionPool.class);

	private static final ReentrantLock lock = new ReentrantLock();

	private static ConnectionPool instance = null;

	private Semaphore semaphore = null;
	
	/*List of free connections in pool */
	private LinkedList<Connection> freeConnections = null;
	
	/*Obtain from properties */
	private String URL;
	private String user;
	private String password;
	
	/*max number of connections in pool */
	private int maxConn;
	/**
	* In constructor we initialize all params and load driver
	* @param URL
	* @param user
	* @param password
	* @param maxConn
	*/
	private ConnectionPool(String URL, String user, String password, int maxConn) {
		this.URL = URL;
		this.user = user;
		this.password = password;
		this.maxConn = maxConn;
		this.semaphore = new Semaphore(maxConn, true);
		this.freeConnections = new LinkedList<Connection>();
		loadDrivers();

		for (int i = 0; i < maxConn; i++) {
			Connection conn = newConnection();
			freeConnections.add(conn);
		}
	}

	private void loadDrivers() {
		try {
			
			Class.forName(ConfigurationManager.getInstance().getProperty(ConfigurationManager.DATABASE_DRIVER_NAME));
		} catch (ClassNotFoundException e) {
			logger.error("Can't load JDBC driver " + e);
		}
	}

	/*ConnectionPool is Singleton */
	public static ConnectionPool getInstance(String URL, String user, String password, int maxConn) {
			if (instance == null) {
				instance = new ConnectionPool(URL, user, password, maxConn);
			}
			return instance;
	}
	/**
	* Create new connection
	* @return Connection
	*/
	private Connection newConnection() {
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL, user, password);
		} catch (SQLException e) {
			logger.error("Can't create a new connection " + e);
			semaphore.release();
		}
		return conn;
	}
	 /**
	*
	* @return free Connection from pool
	*/
	public Connection getConnection() {
		Connection conn = null;
		try {
			semaphore.acquire();
			conn = freeConnections.poll();

		} catch (InterruptedException ex) {
			logger.error(ex.toString());
		}
		return conn;
	}
	/**
	* Add connection to pool
	* @param conn connection, which we need add to pool
	* @throws SQLException if we can't close connection
	*/
	public void freeConnection(Connection conn) throws SQLException { 
		freeConnections.push(conn);
		semaphore.release();
	}
	
	/*Close all connections in pool and clear pool */
	public void release() {
		Iterator<Connection> allConnections = freeConnections.iterator();
		while (allConnections.hasNext()) {
			Connection conn = (Connection) allConnections.next();
			try {
				conn.close();
			} catch (SQLException e) {
				logger.error("Can't close connection for pool " + e);
			}
		}
		freeConnections.clear();
	}
}
