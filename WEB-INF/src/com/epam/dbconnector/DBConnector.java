package com.epam.dbconnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.epam.managers.ConfigurationManager;

public class DBConnector {
	
	private static final Logger logger = Logger.getLogger(DBConnector.class);
	
	private Connection connection;
	private Statement statement;
	private PreparedStatement preparedStatement;
	private ConnectionPool connectionPool;

	/*
	 * Get connection from connection pool
	 */
	public DBConnector() {
		String URL = ConfigurationManager.getInstance().getProperty(ConfigurationManager.DATABASE_URL);
		String user = ConfigurationManager.getInstance().getProperty(ConfigurationManager.DATABASE_USER_NAME);
		String password = ConfigurationManager.getInstance().getProperty(ConfigurationManager.DATABASE_USER_PASSWORD);
		Integer maxConn = Integer
				.parseInt(ConfigurationManager.getInstance().getProperty(ConfigurationManager.DATABASE_MAX_CONN));
		connectionPool = ConnectionPool.getInstance(URL, user, password, maxConn);
		connection = connectionPool.getConnection();
	}

	/*
	 * Get statement from connection
	 */	
	public Statement getStatement() throws SQLException {
		if (connection != null) {
			statement = connection.createStatement();
			if (statement != null) {
				return statement;
			}
		}
		throw new SQLException("Connection or Statement is null");
	}
	/*
	 * Get preparedStatement from connection
	 */	
	public PreparedStatement getPreparedStatement(String sql) throws SQLException {
		if (connection != null) {
			preparedStatement = connection.prepareStatement(sql);
			if (preparedStatement != null) {
				return preparedStatement;
			}
		}
		throw new SQLException("Connection or Statement is null");
	}

	public void close() {
		/* close statement */
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				logger.error("Statement is null " + e);
			}
		}
		/* return this connection to pool */
		if (connection != null) {
			try {
				 connectionPool.freeConnection(connection);
			} catch (SQLException e) {
				 logger.error("Connection is null " + e);
			}
		}
	}
}

