package com.epam.managers;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.epam.command.Command;
import com.epam.command.EmptyCommand;
import com.epam.command.IndexCommand;
import com.epam.command.LoginCommand;
import com.epam.command.admin.AddCourseCommand;
import com.epam.command.admin.AddStudentCommand;
import com.epam.command.admin.AdminCourseDescriptionCommand;
import com.epam.command.admin.DeleteCourseCommand;
import com.epam.command.admin.DeleteStudentCommand;
import com.epam.command.admin.EditCourseCommand;
import com.epam.command.admin.GoToAdminEditCourseCommand;
import com.epam.command.admin.ShowCourseStudentsCommand;
import com.epam.command.student.CourseDescriptionCommand;
import com.epam.command.student.CourseRegisterCommand;
import com.epam.command.student.CourseUnregisterCommand;
import com.epam.command.student.RegistrationCommand;
import com.epam.command.teacher.EditReviewCommand;
import com.epam.command.teacher.GoToEditReviewCommand;
import com.epam.command.teacher.GoToReviewCommand;
import com.epam.command.teacher.ReviewCommand;
import com.epam.command.teacher.TeacherCommand;



public class CommandManager {
	
	/*CommandManager is singleton */
	private static CommandManager instance;
	
	/*The map of all commands(key, value)*/
	private HashMap<String, Command> commands = new HashMap<String, Command>();

	public CommandManager() {
		this.commands.put("index", new IndexCommand());
		this.commands.put("login", new LoginCommand());
		this.commands.put("registration", new RegistrationCommand());
		this.commands.put("courseRegister", new CourseRegisterCommand());
		this.commands.put("addCourse", new AddCourseCommand());
		this.commands.put("courseUnregister", new CourseUnregisterCommand());
		this.commands.put("deleteCourse", new DeleteCourseCommand());
		this.commands.put("editCourse", new EditCourseCommand());
		this.commands.put("teacher", new TeacherCommand());
		this.commands.put("review", new ReviewCommand());
		this.commands.put("editReview", new EditReviewCommand());
		this.commands.put("showCourseStudents", new ShowCourseStudentsCommand());
		this.commands.put("deleteStudent", new DeleteStudentCommand());
		this.commands.put("addStudent", new AddStudentCommand());
		this.commands.put("studentCourseDescription", new CourseDescriptionCommand());
		this.commands.put("goToEditReview", new GoToEditReviewCommand());
		this.commands.put("goToReview", new GoToReviewCommand());
		this.commands.put("adminCourseDescription", new AdminCourseDescriptionCommand());
		this.commands.put("goToAdminEditCourse", new GoToAdminEditCourseCommand());
	}

	public Command getCommand(HttpServletRequest request) {
		
		/*get command from request*/
		String action = request.getParameter("command");
		Command command = commands.get(action);
		
		/*if we have no command*/
		if (command == null) {
			command = new EmptyCommand();
		}
		return command;
	}
	
	/*Singleton pattern*/
	public static CommandManager getInstance() {
		if (instance == null) {
			instance = new CommandManager();
		}
		return instance;
	}
}
