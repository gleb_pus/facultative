package com.epam.managers;

import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class MessageManager {
	
	/*MessageManager is singleton */
	private static MessageManager instance;

	private ResourceBundle resourceBundle;

	private static final String BUNDLE_NAME = "resources.message";
	
	public static final String SERVLET_EXCEPTION_ERROR_MESSAGE = "SERVLET_EXCEPTION_ERROR_MESSAGE";
	public static final String IO_EXCEPTION_ERROR_MESSAGE = "IO_EXCEPTION_ERROR_MESSAGE";
	public static final String LOGIN_ERROR_MESSAGE = "LOGIN_ERROR_MESSAGE";
	public static final String REGISTRATION_ERROR_MESSAGE = "REGISTRATION_ERROR_MESSAGE";
	public static final String REGISTRATION_LOGIN_ERROR_MESSAGE = "REGISTRATION_LOGIN_ERROR_MESSAGE";
	public static final String REGISTRATION_EMAIL_ERROR_MESSAGE = "REGISTRATION_EMAIL_ERROR_MESSAGE";
	public static final String REVIEW_ERROR_MESSAGE = "REVIEW_ERROR_MESSAGE";
	public static final String COURSE_ERROR_MESSAGE = "COURSE_ERROR_MESSAGE";
	public static final String COURSE_TEACHER_ERROR_MESSAGE = "COURSE_TEACHER_ERROR_MESSAGE";
	
	public static MessageManager getInstance() {
		if (instance == null) {
			instance = new MessageManager();
			instance.resourceBundle = PropertyResourceBundle.getBundle(BUNDLE_NAME);
		}
		return instance;
	}
	/**
	* @return property from message.properties
	*/
	public String getProperty(String key) {
		return (String) resourceBundle.getObject(key);
	}
}
