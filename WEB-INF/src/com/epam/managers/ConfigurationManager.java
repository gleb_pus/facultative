package com.epam.managers;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

public class ConfigurationManager {
	
	/*ConfigurationManager is singleton */
	private static ConfigurationManager instance;
	
	private ResourceBundle resourceBundle;

	private static final String BUNDLE_NAME = "resources.config";

	public static final String DATABASE_DRIVER_NAME = "DATABASE_DRIVER_NAME";
	public static final String DATABASE_URL = "DATABASE_URL";
	public static final String DATABASE_USER_NAME = "DATABASE_USER_NAME";
	public static final String DATABASE_USER_PASSWORD = "DATABASE_USER_PASSWORD";
	public static final String DATABASE_MAX_CONN = "DATABASE_MAX_CONN";

	public static final String INDEX_PAGE_PATH = "INDEX_PAGE_PATH";
	public static final String ERROR_PAGE_PATH = "ERROR_PAGE_PATH";
	public static final String LOGIN_PAGE_PATH = "LOGIN_PAGE_PATH";
	public static final String REGISTRATION_PAGE_PATH = "REGISTRATION_PAGE_PATH";
	public static final String SUCCESS_REGISTRATION_PAGE_PATH = "SUCCESS_REGISTRATION_PAGE_PATH";
	public static final String STUDENT_SUCCESS_REGISTER_PAGE_PATH = "STUDENT_SUCCESS_REGISTER_PAGE_PATH";
	public static final String STUDENT_SUCCESS_UNREGISTER_PAGE_PATH = "STUDENT_SUCCESS_UNREGISTER_PAGE_PATH";
	public static final String STUDENT_COURSES_LIST_PAGE_PATH = "STUDENT_COURSES_LIST_PAGE_PATH";
	public static final String STUDENT_COURSE_DESCRIPTION_PAGE_PATH = "STUDENT_COURSE_DESCRIPTION_PAGE_PATH";
	public static final String TEACHER_COURSES_PAGE_PATH = "TEACHER_COURSES_PAGE_PATH";
	public static final String TEACHER_STUDENTS_PAGE_PATH = "TEACHER_STUDENTS_PAGE_PATH";
	public static final String TEACHER_EDIT_REVIEW_PAGE_PATH = "TEACHER_EDIT_REVIEW_PAGE_PATH";
	public static final String TEACHER_REVIEW_PAGE_PATH = "TEACHER_REVIEW_PAGE_PATH";
	public static final String ADMIN_PAGE_PATH = "ADMIN_PAGE_PATH";
	public static final String ADMIN_COURSE_DESCRIPTION_PAGE_PATH = "ADMIN_COURSE_DESCRIPTION_PAGE_PATH";
	public static final String ADMIN_ADD_COURSE_PAGE_PATH = "ADMIN_ADD_COURSE_PAGE_PATH";
	public static final String ADMIN_ADD_STUDENT_PAGE_PATH = "ADMIN_ADD_STUDENT_PAGE_PATH";
	public static final String ADMIN_SUCCESS_DELETE_COURSE_PAGE_PATH = "ADMIN_SUCCESS_DELETE_COURSE_PAGE_PATH";
	public static final String ADMIN_EDIT_COURSE_PAGE_PATH = "ADMIN_EDIT_COURSE_PAGE_PATH";
	public static final String ADMIN_COURSE_STUDENTS_PAGE_PATH = "ADMIN_COURSE_STUDENTS_PAGE_PATH";
	
	public static ConfigurationManager getInstance() {
		if (instance == null) {
			instance = new ConfigurationManager();
			instance.resourceBundle = PropertyResourceBundle.getBundle(BUNDLE_NAME);
		}
		return instance;
	}
	/**
	* @return property from config.properties
	*/
	public String getProperty(String key) {
		return (String) resourceBundle.getObject(key);
	}
}
