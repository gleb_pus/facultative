package com.epam.entity;

import java.util.List;

public class Teacher extends User {
	
	/*List of courses that are taught by teachers*/
	private List<Course> courses;

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}
}
