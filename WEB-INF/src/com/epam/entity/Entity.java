package com.epam.entity;



public abstract class Entity { //implements Serializable, Cloneable 
	private int id;

	public Entity() {
	}

	public Entity(int id) {
		this.id = id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
}
