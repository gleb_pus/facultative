package com.epam.entity;

import java.util.List;

public class Course extends Entity {

	private String name;
	private String description;
	private Teacher teacher;
	
	/*list of students enrolled in a course*/
	private List<Student> students;
	private String beginCourse;
	private String endCourse;

	public Course() {
	}

	public Course(int id, String name, String description, Teacher teacher) {
		super(id);
		this.name = name;
		this.description = description;
		this.teacher = teacher;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	};
	
	public List<Student> getStudents() {
		return students;
	}

	public void setStudents(List<Student> students) {
		this.students = students;
	}

	public String getBeginCourse() {
		return beginCourse;
	}

	public void setBeginCourse(String beginCourse) {
		this.beginCourse = beginCourse;
	}

	public String getEndCourse() {
		return endCourse;
	}

	public void setEndCourse(String endCourse) {
		this.endCourse = endCourse;
	}

}
