package com.epam.entity;

public class UserGroup extends Entity {

	private String name;
	private String code;
	
	public UserGroup(){};
	
	public UserGroup( String name,String code){
		this.name = name;
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

}
