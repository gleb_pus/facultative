package com.epam.entity;

import java.util.List;

public class Student extends User {
	
	/*list of courses for which the student is recorded */
	private List<Course> courses;

	/*list of student reviews */
	private List<Review> reviews;

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public List<Review> getReviews() {
		return reviews;
	}

	public void setReviews(List<Review> reviews) {
		this.reviews = reviews;
	}

}
