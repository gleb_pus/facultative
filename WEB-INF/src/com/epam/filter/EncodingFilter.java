package com.epam.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class EncodingFilter implements Filter {
	
	private String code;

	/*Setting configuration filter*/
	public void init(FilterConfig fConfig) throws ServletException {	
		
		code = fConfig.getInitParameter("encoding");
	}
	/*modification of the encoding*/
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		String codeRequest = request.getCharacterEncoding();
		/*setting the encoding of the filter parameters, if encoding not set*/
		if (code != null && !code.equalsIgnoreCase(codeRequest)) {
			request.setCharacterEncoding(code);
			response.setCharacterEncoding(code);
		}
		chain.doFilter(request, response);
	}

	/*called when the filter is turned off*/
	public void destroy() {
		code = null;
	}
}
