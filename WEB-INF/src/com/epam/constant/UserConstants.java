package com.epam.constant;

public class UserConstants {
	
	public static final String PARAMETER_LOCAL = "local";
	
	public static final String USERGROUP_STUDENT = "student";
	public static final String USERGROUP_TEACHER = "teacher";
	public static final String USERGROUP_ADMIN = "admin";
	
	public static final String PARAMETER_LOGIN = "login";
	public static final String PARAMETER_PASSWORD = "password";
	public static final String PARAMETER_FIRSTNAME = "firstName";
	public static final String PARAMETER_LASTNAME = "lastName";
	public static final String PARAMETER_EMAIL = "email";
	public static final String PARAMETER_RATE = "rate";
	public static final String PARAMETER_REVIEW = "review";
	public static final String PARAMETER_STUDENT = "student";
	
	public static final String ATTRIBUTE_USER = "user";
	public static final String ATTRIBUTE_USER_ID = "userId";
	public static final String ATTRIBUTE_STUDENTS = "students";
	

}
