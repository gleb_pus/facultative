package com.epam.constant;

public class CourseConstants {
	
	public static final String PARAM_NAME_COURSE = "course";
	public static final String PARAM_NAME_NAME = "courseName";
	public static final String PARAM_NAME_DESCRIPTION = "description";
	public static final String PARAM_NAME_TEACHER = "teacher";
	public static final String PARAM_NAME_BEGIN_COURSE = "beginCourse";
	public static final String PARAM_NAME_END_COURSE = "endCourse";
	public static final String PARAM_NAME_COURSE_ID = "courseId";
	public static final String PARAM_NAME_ALL_COURSES = "allCourses";	

}
