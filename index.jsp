<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<html>
<head>
<title>Index JSP</title>
<link type="text/css" rel="stylesheet" href="style/userStyle.css">
<fmt:setLocale value="${sessionScope.local}" />
<fmt:setBundle basename="localization.local" var="loc" />
<fmt:message bundle="${loc}" key="index.message" var="message" />
<fmt:message bundle="${loc}" key="index.enterButton" var="enterButton" />
<fmt:message bundle="${loc}" key="index.locbutton.name.ru" var="ru_button" />
<fmt:message bundle="${loc}" key="index.locbutton.name.en" var="en_button" />
</head>
	<body bgcolor="#CAFF70">
		<form action="${pageContext.request.contextPath}/Controller" method="post">
			<input type="hidden" name="command" value="index" /> 
			<input type="hidden" name="local" value="ru" /> 
			<input type="submit" value="${ru_button}" />
		</form>
		<form action="${pageContext.request.contextPath}/Controller" method="post">
			<input type="hidden" name="command" value="index" /> 
			<input type="hidden" name="local" value="en" /> 
			<input type="submit" value="${en_button}" /><br />
		</form>
		<div class="indexStyle">
			<h1><c:out value="${message}" /></h1>
			</br>
			<h1><a href="/facultative/jsp/login.jsp">${enterButton}</a></h1>
		</div>
	</body>
</html>